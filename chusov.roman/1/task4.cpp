#include <task4.h>

void Task4::fillRandom(double *array, int size)
{
    if(array == NULL)
    {
        std::cout << "array is NULL";
        return;
    }
    if(size <= 0)
    {
        std::cout << "size < 0";
        return;
    }

    for(unsigned int i = 0; i < size; ++i)
    {
        array[i] = ((double)rand() / RAND_MAX) * 2 - 1;
    }
}

void Task4::startTask4(void)
{
    double arr5[5], arr10[10], arr25[25], arr50[50], arr100[100];

    fillRandom(arr5, 5);
    fillRandom(arr10, 10);
    fillRandom(arr25, 25);
    fillRandom(arr50, 50);
    fillRandom(arr100, 100);

    std::vector< std::vector<double> > vect(5);

    vect[0].assign(arr5, arr5 + 5);
    vect[1].assign(arr10, arr10 + 10);
    vect[2].assign(arr25, arr25 + 25);
    vect[3].assign(arr50, arr50 + 50);
    vect[4].assign(arr100, arr100 + 100);

    //Вывожу только вектор из 5-ти
    for(std::vector<double>::iterator it = vect[0].begin(); it != vect[0].end(); ++it)
    {
        std::cout << *it << " ";
    }

    //SortingType< std::vector<double>, SortVectorWithBrackets<double> >::sort(vect[0]);

    std::cout << std::endl;
    for(std::vector<double>::iterator it = vect[0].begin(); it != vect[0].end(); ++it)
    {
        std::cout << *it << " ";
    }
}
