TEMPLATE = app
CONFIG += console c++03
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    task2.cpp \
    task3.cpp \
    task4.cpp

HEADERS += \
    slist.h \
    task1_test.h \
    task2.h \
    task3.h \
    task4.h \
    task1.h

DISTFILES += \
    ../build-lab1-Desktop_Qt_5_5_1_GCC_64bit-Debug/file.txt
