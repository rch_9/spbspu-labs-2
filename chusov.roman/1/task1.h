#ifndef TASK1
#define TASK1
#include <algorithm>

template < template < typename > class Policy, typename Container >
void sortContainer(Container & cont)
{
    typedef Policy<Container> policy_type;
    typedef typename policy_type::this_counter_type counter_type;

    for(counter_type i = policy_type::begin(cont); i != policy_type::end(cont); ++i)
    {
        for(counter_type j = i; j != policy_type::end(cont); ++j)
        {
            if(policy_type::getValue(cont, i) > policy_type::getValue(cont, j))
            {
                std::swap(policy_type::getValue(cont, i), policy_type::getValue(cont, j));
            }
        }
    }
}

template <typename Container>
struct BracketsPolicy
{
    typedef typename Container::size_type this_counter_type;
    typedef typename Container::value_type this_value_type;

    static this_counter_type begin(const Container &);
    static this_counter_type end(const Container & cont);
    static this_value_type & getValue(Container & cont, const this_counter_type pos);
};

template <typename Container>
struct AtPolicy
{
    typedef typename Container::size_type this_counter_type;
    typedef typename Container::value_type this_value_type;

    static this_counter_type begin(const Container &);
    static this_counter_type end(const Container & cont);
    static this_value_type & getValue(Container & cont, const this_counter_type pos);
};


template <typename Container>
struct IteratorsPolicy
{
    typedef typename Container::iterator this_counter_type;
    typedef typename Container::value_type this_value_type;

    static this_counter_type begin(Container &cont);
    static this_counter_type end(Container & cont);
    static this_value_type & getValue(const Container &, const this_counter_type pos);
};


//BracketsPolicy
template <typename Container>
typename BracketsPolicy<Container>::this_counter_type BracketsPolicy<Container>::begin(const Container &)
{
    return 0;
}

template <typename Container>
typename BracketsPolicy<Container>::this_counter_type BracketsPolicy<Container>::end(const Container & cont)
{
    return cont.size();
}

template <typename Container>
typename BracketsPolicy<Container>::this_value_type & BracketsPolicy<Container>::getValue(Container & cont, const this_counter_type pos)
{
    return cont[pos];
}


//AtPolicy
template <typename Container>
typename AtPolicy<Container>::this_counter_type AtPolicy<Container>::begin(const Container &)
{
    return 0;
}

template <typename Container>
typename AtPolicy<Container>::this_counter_type AtPolicy<Container>::end(const Container & cont)
{
    return cont.size();
}

template <typename Container>
typename AtPolicy<Container>::this_value_type & AtPolicy<Container>::getValue(Container & cont, const this_counter_type pos)
{
    return cont.at(pos);
}

//IteratorsPolicy
template <typename Container>
typename IteratorsPolicy<Container>::this_counter_type IteratorsPolicy<Container>::begin(Container & cont)
{
    return cont.begin();
}

template <typename Container>
typename IteratorsPolicy<Container>::this_counter_type IteratorsPolicy<Container>::end(Container & cont)
{
    return cont.end();
}

template <typename Container>
typename IteratorsPolicy<Container>::this_value_type & IteratorsPolicy<Container>::getValue(const Container &, const this_counter_type pos)
{
    return *pos;
}
#endif // TASK1

