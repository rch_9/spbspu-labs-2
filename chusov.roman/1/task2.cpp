#include "task2.h"
#include <fstream>
#include <iostream>

void initVectorFromFile(const char * fileName, std::vector<char> & vect)
{
    std::ifstream fin(fileName);

    char a;
    int fileSize = 0;

    if(!fin.is_open())
    {
        std::cerr << "error" << std::endl;
        return;
    }
    while(fin.good())
    {                
        fin >> a;        
        ++fileSize;
    }
    fin.close();

    fin.open(fileName);

    char array[fileSize];
    for(int i = 0; i < fileSize; ++i)
    {
        fin >> array[i];
    }

    vect.assign(array, array + fileSize);
}
