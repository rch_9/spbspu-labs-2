#ifndef TEST_SORT
#define TEST_SORT

#include "task1.h"
#include <algorithm>
#include <iostream>

template < template < typename > class Policy, typename Container >
bool checkSort(Container container)
{
    Container testContainer = container;

    std::sort(testContainer.begin(), testContainer.end());
    sortContainer<Policy>(container);

    return container == testContainer;
}


template < typename Container >
void printIntegerContainer(Container & container)
{
    for(typename Container::iterator it = container.begin(); it != container.end(); ++it)
    {
        std::cout << *it << " ";
    }
    std::cout << std::endl;

}

#endif // TEST_SORT

