#ifndef MY_LIST
#define MY_LIST

#include <iostream>
#include <assert.h>

template <typename T> class slist
{
    struct node_t
    {
        T value;
        node_t * next;
    };

public:
    typedef T value_type;

    class iterator : public std::iterator<std::forward_iterator_tag, T>
    {
    public:

        iterator() :
            current_(0)
        {}

        iterator(node_t * node) :
            current_(node)
        {}

        bool operator ==(const iterator & rhs) const
        {
            return current_ == rhs.current_;
        }

        bool operator !=(const iterator & rhs) const
        {
            return current_ != rhs.current_;
        }

        T & operator *() const
        {
            assert(current_ != 0);
            return current_->value;
        }

        T& operator ->() const
        {
            assert(current_ != 0);
            return &current_->value;
        }

        iterator& operator ++()
        {
            assert(current_);
            current_ = current_->next;

            return *this;
        }

        iterator& operator ++(int)
        {
            iterator temp(*this);
            this->operator ++();

            return temp;
        }

    private:
        node_t * current_;
    };


    slist():
        begin_(0)
    {}

    ~slist()
    {
        while(begin_)
        {
            node_t * current = begin_->next;
            delete begin_;
            begin_ = current;
        }
    }

    iterator begin()
    {
        return iterator(begin_);
    }

    iterator end()
    {
        return iterator(end_);
    }

    void push_back(T value)
    {
        node_t * temp = new node_t;
        temp->value = value;
        if(!begin_)
        {
            begin_ = temp;
            temp->next = end_ = 0;

        }
        else
        {
            node_t * ind_ptr;            
            for(ind_ptr = begin_; ind_ptr->next != 0; ind_ptr = ind_ptr->next);

            ind_ptr->next = temp;
            temp->next = 0;
            end_ = temp->next;
        }
    }

private:
    node_t * begin_, * end_;
};

#endif // MY_LIST


