#include "factorials.h"
#include <stdexcept>

#define NEXT_AFTER_MAX_VALUE 39916800

const Factorials::iterator Factorials::begin()
{
    return iterator(1);
}

const Factorials::iterator Factorials::end()
{
    return iterator(NEXT_AFTER_MAX_PTR);
}

Factorials::iterator::iterator() :
    currentPtr_(1),
    currentVal_(1)
{}

Factorials::iterator::iterator(unsigned int n) :
    currentPtr_(n)
{
    if(n == 1)
    {
        currentVal_ = 1;
    }
    else if(n == NEXT_AFTER_MAX_PTR)
    {
        currentVal_ = NEXT_AFTER_MAX_VALUE;
    }
}

bool Factorials::iterator::operator ==(const iterator & rhs) const
{
    return currentPtr_ == rhs.currentPtr_;
}

bool Factorials::iterator::operator !=(const iterator & rhs) const
{
    return currentPtr_ != rhs.currentPtr_;
}

unsigned int Factorials::iterator::operator*() const
{    
    if(currentPtr_ == NEXT_AFTER_MAX_PTR)
    {
        throw std::out_of_range("* out of range");
    }

    return currentVal_;
}

Factorials::iterator & Factorials::iterator::operator ++()
{
    if(currentPtr_ == NEXT_AFTER_MAX_PTR)
    {
        throw std::out_of_range("++ out of range");
    }

    ++currentPtr_;
    currentVal_ *= currentPtr_;

    return *this;
}


Factorials::iterator Factorials::iterator::operator ++(int)
{
    iterator temp(*this);
    this->operator ++();

    return temp;
}

Factorials::iterator & Factorials::iterator::operator --()
{        
    if(currentPtr_ == 1)
    {
        throw std::out_of_range("-- out of range");
    }

    --currentPtr_;
    currentVal_ /= (currentPtr_ + 1);

    return *this;
}

Factorials::iterator Factorials::iterator::operator --(int)
{
    iterator temp(*this);
    this->operator --();

    return temp;
}

