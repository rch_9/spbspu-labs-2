#include <vector>
#include <algorithm>
#include "notebook.h"
#include "factorials.h"
#include "factorial-test.h"
#include <stdexcept>

#define BOOST_TEST_MODULE MyTest
#include <boost/test/included/unit_test.hpp>


BOOST_AUTO_TEST_CASE(notebook_test_throw)
{
    Notebook notebook;
    Notebook::iterator it = notebook.begin();

    BOOST_CHECK_THROW(notebook.modify(it, "op", "ne", Notebook::Modificator::replace), std::out_of_range);
    BOOST_CHECK_THROW(notebook.modify(it, "op", "ne", Notebook::Modificator::insert_back), std::out_of_range);
}

BOOST_AUTO_TEST_CASE(notebook_test_push_back)
{
    Notebook notebook;
    Notebook::iterator it = notebook.begin();

    notebook.modify(it, "da", "ne", Notebook::Modificator::push_back);
    BOOST_CHECK(it->getName() == "da");

    notebook.modify(it, "lo", "ne", Notebook::Modificator::push_back);
    ++it;
    BOOST_CHECK(it->getName() == "lo");
}

BOOST_AUTO_TEST_CASE(notebook_test_replace)
{
    Notebook notebook;
    Notebook::iterator it = notebook.begin();

    notebook.modify(it, "da", "ne", Notebook::Modificator::push_back);
    notebook.modify(it, "da1", "ne", Notebook::Modificator::replace);
    BOOST_CHECK(it->getName() == "da1");

    notebook.modify(it, "lo1", "ne", Notebook::Modificator::push_back);
    ++it;
    BOOST_CHECK(it->getName() == "lo1");
}

BOOST_AUTO_TEST_CASE(notebook_test_insert_front)
{
    Notebook notebook;
    Notebook::iterator it = notebook.begin();

    notebook.modify(it, "da", "ne", Notebook::Modificator::insert_front);
    BOOST_CHECK(it->getName() == "da");
    notebook.modify(it, "lo", "ne", Notebook::Modificator::insert_front);

    BOOST_CHECK(it->getName() == "lo");
    ++it;
    BOOST_CHECK(it->getName() == "da");
}


BOOST_AUTO_TEST_CASE(notebook_test_insert_back)
{
    Notebook notebook;
    Notebook::iterator it = notebook.begin();

    notebook.modify(it, "da", "ne", Notebook::Modificator::push_back);
    BOOST_CHECK(it->getName() == "da");

    notebook.modify(it, "daq", "ne", Notebook::Modificator::insert_back);
    BOOST_CHECK(it->getName() == "daq");

    --it;
    BOOST_CHECK(it->getName() == "da");

    notebook.modify(it, "dav", "ne", Notebook::Modificator::insert_back);
    BOOST_CHECK(it->getName() == "dav");

    ++it;
    notebook.modify(it, "daq", "ne", Notebook::Modificator::insert_back);
    BOOST_CHECK(it->getName() == "daq");
}


BOOST_AUTO_TEST_CASE(container_test_pre)
{
    Factorials cont;
    int i = 0;

    for(Factorials::iterator it = cont.begin(); it != cont.end(); ++it)
    {
        ++i;
        BOOST_CHECK(*it == factorial(i));
    }
    BOOST_CHECK(i == NEXT_AFTER_MAX_PTR - 1);

    Factorials::iterator it2 = cont.end();
    --it2;
    for(Factorials::iterator it = it2; it != cont.begin(); --it)
    {
        BOOST_CHECK(*it == factorial(i));
        --i;
    }
}

BOOST_AUTO_TEST_CASE(container_test_post)
{
    Factorials cont;
    int i = 0;

    for(Factorials::iterator it = cont.begin(); it != cont.end(); it++)
    {
        ++i;
        BOOST_CHECK(*it == factorial(i));
    }
    BOOST_CHECK(i == NEXT_AFTER_MAX_PTR - 1);

    Factorials::iterator it2 = cont.end();
    it2--;
    for(Factorials::iterator it = it2; it != cont.begin(); it--)
    {
        BOOST_CHECK(*it == factorial(i));
        --i;
    }
}

BOOST_AUTO_TEST_CASE(container_test_copy)
{    
    Factorials cont;
    std::vector < unsigned > vect(cont.begin(), cont.end());
    std::copy(cont.begin(), cont.end(), vect.begin());


    BOOST_CHECK_EQUAL_COLLECTIONS(cont.begin(), cont.end(), vect.begin(), vect.end());
}
