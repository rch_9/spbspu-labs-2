TEMPLATE = app
CONFIG += console c++03
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    notebook.cpp \
    factorials.cpp

HEADERS += \
    notebook.h \
    factorials.h \
    factorial-test.h

