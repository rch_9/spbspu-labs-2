#ifndef FACTORIALS
#define FACTORIALS

#include <iterator>

#define NEXT_AFTER_MAX_PTR 11

struct Factorials
{
    class iterator : public std::iterator < std::bidirectional_iterator_tag, unsigned int >
    {
        friend class Factorials;

        iterator();
        iterator(unsigned int n);

    public:
        bool operator ==(const iterator & rhs) const;
        bool operator !=(const iterator & rhs) const;

        unsigned int operator*() const;

        iterator & operator ++();
        iterator operator ++(int);
        iterator & operator --();
        iterator operator --(int);

    private:        
        unsigned int currentPtr_, currentVal_;
    };

    const iterator begin();
    const iterator end();
};

#endif // FACTORIALS

