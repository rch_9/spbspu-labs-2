#include <cmath>
#include "notebook.h"
#include <stdexcept>

Notebook::Record::Record(ConstString & name, ConstString & phoneNumber):
    data(std::make_pair(name, phoneNumber))
{}

const std::string & Notebook::Record::getName() const
{
    return data.first;
}

const std::string & Notebook::Record::getPhoneNumber() const
{
    return data.second;
}

Notebook::Notebook()
{}

const Notebook::iterator Notebook::begin()
{
    return container_.begin();
}

const Notebook::iterator Notebook::end()
{
    return container_.end();
}

void Notebook::modify(iterator & current, ConstString &name, ConstString &phoneNumber,
                                    void (*modyfyRecord)(iterator &, ConstString &, ConstString &, Container &))
{
    modyfyRecord(current, name, phoneNumber, container_);
}

void Notebook::Modificator::push_back(iterator & current, ConstString & name, ConstString & phoneNumber, Container & container)
{
    container.push_back(Record(name, phoneNumber));
    current = container.begin();
}

void Notebook::Modificator::replace(iterator & current, ConstString & name, ConstString & phoneNumber, Container & container)
{
    if(current == container.end())
    {
        throw std::out_of_range("repace is out of range");
    }

    *current = Record(name, phoneNumber);
}

void Notebook::Modificator::insert_front(iterator & current, ConstString & name, ConstString & phoneNumber, Container & container)
{
    current = container.insert(current, Record(name, phoneNumber));
}

void Notebook::Modificator::insert_back(iterator & current, ConstString & name, ConstString & phoneNumber, Container & container)
{    
    if(current == container.end())
    {
        throw std::out_of_range("insert_back is out of range");
    }

    current = container.insert(current + 1, Record(name, phoneNumber));
}
