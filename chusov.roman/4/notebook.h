#ifndef NOTEBOOK
#define NOTEBOOK

#include <string>
#include <vector>
//#include <list>
#include <utility>

class Notebook
{
    Notebook(const Notebook &);
    Notebook & operator=(Notebook &);

    typedef const std::string ConstString;

    struct Record
    {
        Record(ConstString & name, ConstString & phoneNumber);        

        const std::string & getName() const;
        const std::string & getPhoneNumber() const;
    private:
        std::pair<std::string, std::string> data;
    };

    //этот вектор можно изменять на list, всё должно работать
    typedef std::vector < Record > Container;    

public:    
    typedef Container::iterator iterator;
    Notebook();

    const iterator begin();
    const iterator end();

    void modify(iterator & current, ConstString & name, ConstString & phoneNumber,
                    void (*modyfyRecord)(iterator &, ConstString &, ConstString &, Container &));

    class Modificator
    {
        Modificator();
        Modificator(const Modificator &);
        const Modificator & operator =(const Modificator &);

    public:
        static void push_back(iterator &current, ConstString & name, ConstString & phoneNumber, Container &container);
        static void replace(iterator & current, ConstString & name, ConstString & phoneNumber, Container &container);
        static void insert_front(iterator & current, ConstString & name, ConstString & phoneNumber, Container &container);
        static void insert_back(iterator & current, ConstString & name, ConstString & phoneNumber, Container &container);
    };

private:   
    Container container_;
};

#endif // NOTEBOOK


