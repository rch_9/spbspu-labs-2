#ifndef FACTORIALTEST
#define FACTORIALTEST

#include "factorials.h"

unsigned factorial(int n)
{
    if(n == 0)
    {
        return 1;
    }

    for(int i = n - 1; i > 0; --i)
    {
        n*=i;
    }

    return n;
}

#endif // FACTORIALTEST

