#ifndef FIGURES
#define FIGURES

#include <vector>
#include <string>

//подразумевается отдельной фигурой
struct Point
{
    static const unsigned int RANGE = 10;

    Point();
    Point(int xx, int yy);

    int x , y;
};


class ShapeType
{
    ShapeType();
    ShapeType(const ShapeType & type);
    ShapeType operator =(const ShapeType & type);    

public:
    typedef enum
    {
        TRIANGLE = 1,
        SQUARE,
        RECTANGLE,
        PENTAGON
    } TYPE;        

    //при добавлении новых фигур нужно увеличивать AMOUNT_TYPES
    static const unsigned int AMOUNT_TYPES = 4;
};


typedef std::vector<Point> PointsContainer;
/*****************************************/


struct Shape
{
    virtual ~Shape() {}

    const ShapeType::TYPE & getType() const;
    const Point & getPoint(const int n) const;
    const std::string & getName() const;
    const unsigned int & getVertexNum() const;

protected:
    Shape() {}
    void fillByPoints();

    unsigned int vertexNum_;
    PointsContainer vertexes_;
    ShapeType::TYPE type_;
    std::string name_;
};


typedef std::vector<Shape> ShapesContainer;
/*****************************************/


struct Triangle : public Shape
{
    Triangle();
};

struct Square : public Shape
{
    Square();
};

struct Rectangle : public Shape
{
    Rectangle();
};

struct Pentagon : public Shape
{
    Pentagon();
};

#endif // FIGURES

