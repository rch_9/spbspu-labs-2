#ifndef TASK1
#define TASK1

#include <algorithm>
#include <fstream>
#include <stdexcept>
#include <set>
#include <iostream>
#include <iterator>
#include <string>

void task1(const char * fileName)
{    
    std::set<std::string> setOfStr;

    std::ifstream fin(fileName);

    if(!fin.is_open())
    {        
        throw std::runtime_error("\nfile did not opened\n");
    }            

    std::copy(std::istream_iterator<std::string>(fin), std::istream_iterator<std::string>(),
              std::inserter(setOfStr, setOfStr.begin()));

    std::copy(setOfStr.begin(), setOfStr.end(), std::ostream_iterator<std::string>(std::cout, " "));
}

#endif // TASK1

