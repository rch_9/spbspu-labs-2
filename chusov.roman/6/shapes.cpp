#include <stdexcept>
#include <cstdlib>
#include "shapes.h"

Point::Point():
    x(0),
    y(0)
{}

Point::Point(int xx, int yy):
    x(xx),
    y(yy)
{}

const unsigned int & Shape::getVertexNum() const
{
    return vertexNum_;
}

void Shape::fillByPoints()
{
    for(unsigned int i = 0; i < vertexNum_; ++i)
    {
        vertexes_.push_back(Point(rand() % Point::RANGE, rand() % Point::RANGE));
    }
}

const Point & Shape::getPoint(const int n) const
{
    if(n < 0 || n >= static_cast< int >(vertexNum_))
    {
        throw std::out_of_range("point is out of range");
    }

    return vertexes_.at(n);
}

const std::string & Shape::getName() const
{
    return name_;
}

const ShapeType::TYPE & Shape::getType() const
{
    return type_;
}

Triangle::Triangle()
{
    vertexNum_ = 3;
    type_ = ShapeType::TRIANGLE;
    name_ = "tria";
    fillByPoints();
}

Square::Square()
{    
    vertexNum_ = 4;
    type_ = ShapeType::SQUARE;
    name_ = "squa";
    fillByPoints();
}

Rectangle::Rectangle()
{    
    vertexNum_ = 4;
    type_ = ShapeType::RECTANGLE;
    name_ = "rect";
    fillByPoints();
}

Pentagon::Pentagon()
{
    vertexNum_ = 5;
    type_ = ShapeType::PENTAGON;
    name_ = "pent";
    fillByPoints();
}
