TEMPLATE = app
CONFIG += console c++03
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    shapes.cpp \
    funstors.cpp \
    myalgorithms.cpp

HEADERS += \
    task1.h \
    shapes.h \
    functors.h \
    myalgorithms.h

DISTFILES += \
    ../build-6-Desktop_Qt_5_5_1_GCC_64bit-Debug/Inputfile.txt

