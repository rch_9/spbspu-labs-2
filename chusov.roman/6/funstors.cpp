#include "functors.h"
#include <cstdlib>
#include <iostream>

vertexes_counter::vertexes_counter():
        counter_(0)
{}

void vertexes_counter::operator()(const Shape & shape)
{
    counter_ += shape.getVertexNum();
}

unsigned int vertexes_counter::getCounter() const
{
    return counter_;
}


certain_shapes_counter::certain_shapes_counter():
    counter_(0)
{}

void certain_shapes_counter::operator()(const Shape & shape)
{
    if(shape.getType() == 1 || shape.getType() == 2 || shape.getType() == 3)
    { ++counter_; }
}

unsigned int certain_shapes_counter::getCounter() const
{
    return counter_;
}


bool is_pentagon(const Shape & shape)
{
    return shape.getType() == 4;
}


const Point &get_point(const Shape & shape)
{
    return shape.getPoint(rand() % shape.getVertexNum());
}


void print_shape(const Shape & shape)
{
    std::cout << shape.getName() << "\t";
}


void print_point(const Point & point)
{
    std::cout << point.x << "_" << point.y << "\t";
}


bool is_less_left_shape(const Shape & lhs, const Shape & rhs)
{
    return lhs.getType() < rhs.getType();
}
