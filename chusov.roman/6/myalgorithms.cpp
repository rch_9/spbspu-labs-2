#include <iostream>
#include <algorithm>
#include "myalgorithms.h"
#include "functors.h"

unsigned int considerVertexes(const ShapesContainer & sh_cont)
{
    vertexes_counter counter = std::for_each(sh_cont.begin(), sh_cont.end(), vertexes_counter());

    return counter.getCounter();
}

unsigned int considerCertainShapes(const ShapesContainer & sh_cont)
{
    certain_shapes_counter counter = std::for_each(sh_cont.begin(), sh_cont.end(), certain_shapes_counter());

    return counter.getCounter();
}

void removePentagons(ShapesContainer & sh_cont)
{
    sh_cont.erase(std::remove_if(sh_cont.begin(), sh_cont.end(), is_pentagon), sh_cont.end());
}

void fillPointsContainer(const ShapesContainer & sh_cont, PointsContainer & sh_cont2)
{
    sh_cont2.resize(sh_cont.size());
    std::transform(sh_cont.begin(), sh_cont.end(), sh_cont2.begin(), get_point);
}

void sort(ShapesContainer & sh_cont)
{
    std::sort(sh_cont.begin(), sh_cont.end(), is_less_left_shape);
}

void print(const ShapesContainer & sh_cont)
{
    std::for_each(sh_cont.begin(), sh_cont.end(), print_shape);
    std::cout << std::endl;
}

void print(const PointsContainer & po_cont)
{
    std::for_each(po_cont.begin(), po_cont.end(), print_point);
    std::cout << std::endl;
}
