#ifndef MYALGORITHMS
#define MYALGORITHMS

#include "shapes.h"

unsigned int considerVertexes(const ShapesContainer & sh_cont);

unsigned int considerCertainShapes(const ShapesContainer & sh_cont);

void removePentagons(ShapesContainer & sh_cont);

void fillPointsContainer(const ShapesContainer & sh_cont, PointsContainer & sh_contOfPoints);

void sort(ShapesContainer & sh_cont);

void print(const ShapesContainer & sh_cont);

void print(const PointsContainer & po_cont);


#endif // MYALGORITHMS_TASK2
