#ifndef FUNCTORS
#define FUNCTORS

#include "shapes.h"

struct vertexes_counter
{
    vertexes_counter();
    void operator()(const Shape & shape);
    unsigned int getCounter() const;

private:
    unsigned int counter_;
};

struct certain_shapes_counter
{
    certain_shapes_counter();
    void operator()(const Shape & shape);
    unsigned int getCounter() const;

private:
    unsigned int counter_;
};

bool is_pentagon(const Shape & shape);

const Point & get_point(const Shape & shape);

void print_shape(const Shape & shape);

void print_point(const Point & point);

bool is_less_left_shape(const Shape & lhs, const Shape & rhs);


#endif // FUNCTORS

