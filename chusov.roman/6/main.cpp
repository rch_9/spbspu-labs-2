#include <iostream>
#include "task1.h"
#include "shapes.h"
#include "myalgorithms.h"

#define BOOST_TEST_MODULE MyTest
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE(task1_test)
{
    BOOST_CHECK_THROW(task1("Inputfile.tx"), std::runtime_error);
    task1("Inputfile.txt");
    std::cout << std::endl << std::endl;
}


BOOST_AUTO_TEST_CASE(task2_algo_test)
{
    srand(time(0));

    ShapesContainer shapes;

    shapes.push_back(Pentagon());
    shapes.push_back(Triangle());
    shapes.push_back(Square());
    shapes.push_back(Rectangle());
    shapes.push_back(Pentagon());
    shapes.push_back(Triangle());
    shapes.push_back(Square());
    shapes.push_back(Rectangle());
    shapes.push_back(Pentagon());

    print(shapes);

    BOOST_CHECK(considerVertexes(shapes) == 37);
    BOOST_CHECK(considerCertainShapes(shapes) == 6);
    BOOST_CHECK_THROW(shapes.at(0).getPoint(20), std::out_of_range);

    removePentagons(shapes);
    print(shapes);


    PointsContainer points;
    fillPointsContainer(shapes, points);
    print(points);

    sort(shapes);
    print(shapes);
}

BOOST_AUTO_TEST_CASE(task2_algo_test_0_shapes)
{
    ShapesContainer shapes;
    print(shapes);

    BOOST_CHECK_THROW(shapes.at(0).getPoint(1), std::out_of_range);

    BOOST_CHECK(considerVertexes(shapes) == 0);
    BOOST_CHECK(considerCertainShapes(shapes) == 0);
}


BOOST_AUTO_TEST_CASE(task2_algo_test_1_shapes_rect)
{
    ShapesContainer shapes;

    shapes.push_back(Rectangle());
    print(shapes);

    BOOST_CHECK(considerVertexes(shapes) == 4);
    BOOST_CHECK(considerCertainShapes(shapes) == 1);

    sort(shapes);
    print(shapes);
}

BOOST_AUTO_TEST_CASE(task2_algo_test_1_shapes_pent)
{
    ShapesContainer shapes;

    shapes.push_back(Pentagon());
    print(shapes);

    BOOST_CHECK(considerVertexes(shapes) == 5);
    BOOST_CHECK(considerCertainShapes(shapes) == 0);

    sort(shapes);
    print(shapes);

    removePentagons(shapes);

    BOOST_CHECK(considerVertexes(shapes) == 0);
    BOOST_CHECK(considerCertainShapes(shapes) == 0);

    print(shapes);
}
