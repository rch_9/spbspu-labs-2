#ifndef PRIORITYQUEUE
#define PRIORITYQUEUE

#include <list>

template < typename T >
struct PriorityQueue
{ 
    struct NodePriority
    {
        enum PRIORITY
        {
            LOW = 1,
            NORMAL,
            HIGH
        };

    private:
        NodePriority();
        NodePriority(const NodePriority &);
        NodePriority operator=(const NodePriority &);
    };

private:
    typedef typename NodePriority::PRIORITY Priority;
    typedef const Priority ConstPriority;
    typedef const T ConstT;

    struct Node
    {
        Node(ConstT & data, ConstPriority & priority);

        Priority priority_;
        T data_;
    };

    typedef std::list< Node > NodeList;

public:
    void push(ConstT & data, ConstPriority & priority);
    T get_top();

    unsigned int size() const;
    bool empty() const;

    void accelerate();

private:
    T eraseTopNode(NodeList & nodeList);
    NodeList lowList_, normalList_, highList_;
};


template < typename T >
void PriorityQueue<T>::push(ConstT & data, ConstPriority & priority)
{
    switch (priority)
    {
    case NodePriority::HIGH:
        highList_.push_back(Node(data, priority));
        break;
    case NodePriority::NORMAL:
        normalList_.push_back(Node(data, priority));
        break;
    case NodePriority::LOW:
        lowList_.push_back(Node(data, priority));
    }
}

template < typename T >
T PriorityQueue<T>::eraseTopNode(NodeList & nodeList)
{
    T result;
    result = nodeList.front().data_;
    nodeList.erase(nodeList.begin());

    return result;
}

template < typename T >
T PriorityQueue<T>::get_top()
{
    if(!highList_.empty())
    {
        return eraseTopNode(highList_);
    }

    if(!normalList_.empty())
    {
        return eraseTopNode(normalList_);
    }

    return eraseTopNode(lowList_);
}

template < typename T >
unsigned int PriorityQueue<T>::size() const
{
    return lowList_.size() + normalList_.size() + highList_.size();
}


template < typename T >
bool PriorityQueue<T>::empty() const
{
    return highList_.empty() && normalList_.empty() && lowList_.empty();
}


template < typename T >
void PriorityQueue<T>::accelerate()
{
    for(typename NodeList::iterator it = lowList_.begin(); it != lowList_.end(); ++it)
    { it->priority_ = NodePriority::HIGH; }

    highList_.splice(highList_.end(), lowList_);
}


template < typename T >
PriorityQueue<T>::Node::Node(ConstT & data, ConstPriority & priority):
    priority_(priority),
    data_(data)
{}

#endif // PRIORITYQUEUE

