TEMPLATE = app
CONFIG += console c++03
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

HEADERS += \
    task2.h \
    task2_test.h \
    priorityqueue.h \
    priorityqueue_test.h

