#include <ctime>
#include "priorityqueue.h"
#include "priorityqueue_test.h"
#include "task2.h"
#include "task2_test.h"

#define BOOST_TEST_MODULE MyTest
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE(task1)
{    
    BOOST_CHECK(PriorityQueueTest::push_top_pop() == "243516");
    BOOST_CHECK(PriorityQueueTest::accelerate() == "241635");   
}

BOOST_AUTO_TEST_CASE(task2)
{
    srand(static_cast< unsigned int >(time(0)));
    IntList list;

    std::stringstream strStream;

    BOOST_CHECK_THROW(fillRandom(list, -30), std::out_of_range);
    BOOST_CHECK_THROW(fillRandom(list, 30), std::out_of_range);
    BOOST_CHECK_THROW(putInUnusualOrder(list, strStream), std::out_of_range);
    BOOST_CHECK(Task2Test::checkPutinUnusualOrder());
}

