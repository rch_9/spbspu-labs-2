#ifndef TASK2_TEST
#define TASK2_TEST
#include "task2.h"

namespace Task2Test
{    
    //на четной и нечетной длине списка
    bool checkPutinUnusualOrder()
    {
        IntList l1;
        for(int i = 0; i < 10; ++i)
        { l1.push_back(i); }

        IntList l2;
        for(int i = 1; i < 10; ++i)
        { l2.push_back(i); }

        std::stringstream strStream1, strStream2;
        putInUnusualOrder(l1, strStream1);
        putInUnusualOrder(l2, strStream2);

        if((strStream1.str() == "0918273645") && (strStream2.str() == "192837465"))
        { return true; }

        return false;
    }
}
#endif // TASK2_TEST

