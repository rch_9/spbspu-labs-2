#ifndef PRIORITYQUEUE_TEST
#define PRIORITYQUEUE_TEST

#include <string>
#include "priorityqueue.h"

namespace PriorityQueueTest
{
    typedef PriorityQueue<std::string> StrPriorityQueue;

    void init(StrPriorityQueue & q)
    {
        q.push("1", StrPriorityQueue::NodePriority::LOW);
        q.push("2", StrPriorityQueue::NodePriority::HIGH);
        q.push("3", StrPriorityQueue::NodePriority::NORMAL);
        q.push("4", StrPriorityQueue::NodePriority::HIGH);
        q.push("5", StrPriorityQueue::NodePriority::NORMAL);
        q.push("6", StrPriorityQueue::NodePriority::LOW);
    }

    std::string show(StrPriorityQueue & q)
    {
        unsigned int size = q.size();
        std::string str;

        for(unsigned int i = 0; i < size; ++i)
        {
            str+=q.get_top();
        }

        return str;
    }

    std::string push_top_pop()
    {
        StrPriorityQueue queue;
        init(queue);

        return show(queue);
    }

    std::string accelerate()
    {
        StrPriorityQueue queue;
        init(queue);
        queue.accelerate();

        return show(queue);
    }
}


#endif // PRIORITYQUEUE_TEST

