#ifndef TASK2
#define TASK2

#include <list>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <stdexcept>

#define MAX_RAND_VALUE 20
#define MAX_SIZE 20

typedef std::list<int> IntList;

void fillRandom(IntList & list, const int size)
{

    if((size <= 0) || (size > MAX_SIZE))
    { throw std::out_of_range("20 < count <= 0"); }

    list.clear();

    for(int i = 0; i < size; ++i)
    { list.push_back(1 + (unsigned int)rand() % MAX_RAND_VALUE); }
}

void putInUnusualOrder(IntList & list, std::stringstream & strStream)
{
    if(list.empty())
    { throw std::out_of_range("list is empty"); }    

    IntList::iterator itf = list.begin(), itb = list.end();    
    while(itf != itb)
    {
        strStream << *itf;
        ++itf;

        if(itf != itb)
        {
            --itb;
            strStream << *itb;
        }
    }
}


#endif // TASK2

