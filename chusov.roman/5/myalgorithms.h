#ifndef MYALGORITHMS
#define MYALGORITHMS

#include <iostream>
#include <vector>
#include <stdexcept>
#include "datastruct.h"

//модуль максимального рандомного
#define ABS_MAX_RAND 5

inline void print_data(const DataStruct & obj)
{
    std::cout << obj << std::endl;
}

inline void equalize_key1(DataStruct & obj)
{
    obj.key1_ = 0;
}

inline void equalize_key2(DataStruct & obj)
{
    obj.key2_ = 0;
}

typedef std::vector < DataStruct > VectorOfDataStruct;
typedef std::vector < std::string > Table;

inline void fill_random(const unsigned int n, VectorOfDataStruct & v,const Table & t)
{
    if(t.empty())
    {        
        throw std::length_error("\nTable is empty\n\n");
    }

    v.clear();

    for(unsigned int i = 0; i < n; ++i)
    {
        v.push_back(DataStruct(rand() % (ABS_MAX_RAND * 2 + 1) - ABS_MAX_RAND,
                               rand() % (ABS_MAX_RAND * 2 + 1) - ABS_MAX_RAND,
                               t[ rand() % t.size() ]));
    }
}

#endif // MYALGORITHMS

