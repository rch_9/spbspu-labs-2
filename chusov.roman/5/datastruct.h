#ifndef DATASTRUCT
#define DATASTRUCT

#include <string>
#include <ostream>

struct DataStruct
{
    DataStruct(int key1, int key2, std::string str);

    int key1_;
    int key2_;
    std::string str_;
};

DataStruct::DataStruct(int key1, int key2, std::string str):
    key1_(key1),
    key2_(key2),
    str_(str)
{}


inline bool operator<(const DataStruct & lhs, const DataStruct & rhs)
{
    if(lhs.key1_ != rhs.key1_)
    {
        return lhs.key1_ < rhs.key1_;
    }
    else if(lhs.key2_ != rhs.key2_)
    {
        return lhs.key2_ < rhs.key2_;
    }    

    return lhs.str_.size() < rhs.str_.size();
}

inline std::ostream & operator<<(std::ostream & os, const DataStruct & obj)
{
    os << obj.key1_ << "\t" << obj.key2_ << "\t" << obj.str_;

    return os;
}

#endif // DATASTRUCT

