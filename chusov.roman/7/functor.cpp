#include <stdexcept>
#include <stdio.h>
#include "functor.h"

#define ABS_RANGE 500

functor::functor(const int & firstNumb):
    firstNumb_(firstNumb),
    minNumb_(firstNumb),
    maxNumb_(firstNumb),
    averageNumb_(0),
    sumOddNumbs_(0),
    sumEvenNumbs_(0),
    amountNegativeNumbs_(0),
    amountPositiveNumbs_(0),
    isLastSameAsFirst(true)
{}


void functor::operator()(const int & numb)
{
    if(numb < -ABS_RANGE || numb > ABS_RANGE)
    {
        throw std::out_of_range("\n\nnumb is out of range\n\n");
    }

    checkOnMinAndMax(numb);
    checkOnOdd(numb);
    checkOnNegative(numb);
    checkOnLastSameAsFirst(numb);
    addAverage();
}


void functor::checkOnMinAndMax(const int & numb)
{
    if(numb < minNumb_)
    { minNumb_ = numb; }
    else if(numb > maxNumb_)
    { maxNumb_ = numb; }
}


void functor::addAverage()
{
    averageNumb_ = (sumEvenNumbs_ + sumOddNumbs_) / static_cast<int>(amountNegativeNumbs_ + amountPositiveNumbs_);
}


void functor::checkOnOdd(const int & numb)
{
    if(numb % 2)
    { sumOddNumbs_ += numb; }
    else
    { sumEvenNumbs_ += numb; }
}


void functor::checkOnNegative(const int & numb)
{
    if(numb < 0)
    { ++amountNegativeNumbs_; }
    else
    { ++amountPositiveNumbs_; }
}


void functor::checkOnLastSameAsFirst(const int & numb)
{ isLastSameAsFirst = numb == firstNumb_; }


void functor::showResults() const
{

    printf("\nMax:\t\t\t%d\nMin:\t\t\t%d\nAverage:\t\t%d", maxNumb_, minNumb_, averageNumb_);
    printf("\nPositive:\t\t%d\nNegative:\t\t%d", amountPositiveNumbs_, amountNegativeNumbs_);
    printf("\nSum Odd:\t\t%d\nSum Even:\t\t%d", sumOddNumbs_, sumEvenNumbs_);
    printf("\nIs Last Same As First:\t%d\n\n", isLastSameAsFirst);
}


