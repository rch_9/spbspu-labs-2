#include <algorithm>
#include <stdexcept>
#include <vector>
#include <stdlib.h>
#include <ctime>
#include <stdio.h>
#include "functor.h"

#define ABS_RANGE 500

void fillRandom(std::vector<int> & v, int n)
{
    for(int i = 0; i < n; ++i)
    {
        v.push_back(rand() % ABS_RANGE - ABS_RANGE / 2);
    }
}

int main()
{
    srand(time(0));

    std::vector<int> seq;
    fillRandom(seq, 10);

    try
    {
        functor f = std::for_each(seq.begin(), seq.end(), functor(*seq.begin()));
        f.showResults();
    }
    catch(std::out_of_range & excp)
    {
        perror(excp.what());
    }
}
