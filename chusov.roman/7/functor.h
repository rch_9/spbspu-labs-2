#ifndef FUNCTOR
#define FUNCTOR

struct functor
{
    functor(const int & firstNumb);
    void operator()(const int & numb);

    void showResults() const;
private:
    void checkOnMinAndMax(const int & numb);
    void checkOnOdd(const int & numb);
    void checkOnNegative(const int & numb);
    void checkOnLastSameAsFirst(const int & numb);
    void addAverage();

    int firstNumb_, minNumb_, maxNumb_, averageNumb_, sumOddNumbs_, sumEvenNumbs_;
    unsigned int amountNegativeNumbs_, amountPositiveNumbs_;
    bool isLastSameAsFirst;
};

#endif // FUNCTOR

