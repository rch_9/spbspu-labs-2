#ifndef SHAPES
#define SHAPES

#include <utility>
#include <string>

class Shape
{
    typedef std::pair<int, int> Point;
public:
    virtual ~Shape()
    {} 

    const Point & getCenter() const;

    bool isMoreLeft(const Shape & shape) const;
    bool isUpper(const Shape & shape) const;

    virtual void draw() const = 0;

protected:
    Shape(int xx, int yy);

    std::string name_;
    Point center_;
};


struct Triangle : public Shape
{
    Triangle(int xx, int yy);
    void draw() const;
};

struct Square : public Shape
{
    Square(int xx, int yy);
    void draw() const;
};

struct Circle : public Shape
{
    Circle(int xx, int yy);
    void draw() const;
};

#endif // SHAPES

