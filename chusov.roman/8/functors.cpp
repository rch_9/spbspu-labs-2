#include "functors.h"
#include <stdlib.h>
#include <boost/make_shared.hpp>

#define MAX_COORDINATE 20
#define AMOUNT_SHAPES 3

void print_shape(const boost::shared_ptr<Shape> & pShape)
{
    pShape->draw();
}

ShapeGenerator::pShape ShapeGenerator::operator()()
{
    int x = rand() % MAX_COORDINATE - MAX_COORDINATE / 2;
    int y = rand() % MAX_COORDINATE - MAX_COORDINATE / 2;

    switch(rand() % AMOUNT_SHAPES)
    {
    case 0:
        return boost::make_shared<Triangle>(Triangle(x, y));
    case 1:
        return boost::make_shared<Square>(Square(x, y));
    default:
        return boost::make_shared<Circle>(Circle(x, y));
    }
}

bool ShapeComparatorX::operator()(ConstPShapeRef lhs, ConstPShapeRef rhs)
{
    return lhs->isMoreLeft(*rhs);
}

bool ShapeComparatorY::operator()(ConstPShapeRef lhs, ConstPShapeRef rhs)
{
    return lhs->isUpper(*rhs);
}
