TEMPLATE = app
CONFIG += console c++03
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    shapes.cpp \
    functors.cpp

HEADERS += \
    functors.h \
    sss \
    shapes.h

