#include "shapes.h"
#include "functors.h"
#include <cmath>
#include <vector>
#include <algorithm>
#include <functional>
#include <iterator>
#include <iostream>
#include <time.h>
#include <boost/bind.hpp>

int main()
{        
    srand(time(0));

    //task1
    std::vector<double> v(5, 1);

    std::transform(v.begin(), v.end(), v.begin(), (boost::bind(std::multiplies<double>(), _1, M_PI)));
    std::copy(v.begin(), v.end(), std::ostream_iterator<double>(std::cout, " "));
    std::cout << std::endl;

    //task2
    std::vector < boost::shared_ptr < Shape > > list(5);
    std::generate(list.begin(), list.end(), ShapeGenerator());    

    std::for_each(list.begin(), list.end(), print_shape);

    std::sort(list.begin(), list.end(), (boost::bind(ShapeComparatorX(), _1, _2)));
    std::cout << std::endl;
    std::for_each(list.begin(), list.end(), print_shape);

    std::sort(list.begin(), list.end(), (boost::bind(ShapeComparatorX(), _2, _1)));
    std::cout << std::endl;
    std::for_each(list.begin(), list.end(), print_shape);

    std::sort(list.begin(), list.end(), (boost::bind(ShapeComparatorY(), _1, _2)));
    std::cout << std::endl;
    std::for_each(list.begin(), list.end(), print_shape);

    std::sort(list.begin(), list.end(), (boost::bind(ShapeComparatorY(), _2, _1)));
    std::cout << std::endl;
    std::for_each(list.begin(), list.end(), print_shape);


    std::cout << std::endl;
    return 0;
}

