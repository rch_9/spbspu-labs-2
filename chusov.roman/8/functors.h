#ifndef SHAPEGENERATOR
#define SHAPEGENERATOR

#include "shapes.h"
#include <boost/shared_ptr.hpp>

void print_shape(const boost::shared_ptr<Shape> & pShape);

class ShapeGenerator
{
    typedef boost::shared_ptr<Shape> pShape;

public:
    pShape operator()();
};

class ShapeComparatorX
{
    typedef const boost::shared_ptr<Shape> & ConstPShapeRef;

public:
    typedef bool result_type;
    bool operator()(ConstPShapeRef lhs, ConstPShapeRef rhs);
};

class ShapeComparatorY
{
    typedef const boost::shared_ptr<Shape> & ConstPShapeRef;

public:
    typedef bool result_type;
    bool operator()(ConstPShapeRef lhs, ConstPShapeRef rhs);
};

#endif // SHAPEGENERATOR

