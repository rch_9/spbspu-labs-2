#include "shapes.h"
#include <iostream>

Shape::Shape(int xx, int yy):
    center_(std::make_pair(xx, yy))
{}

bool Shape::isMoreLeft(const Shape & shape) const
{
    return center_.first < shape.getCenter().first;
}

bool Shape::isUpper(const Shape & shape) const
{
    return center_.second > shape.getCenter().second;
}

const Shape::Point & Shape::getCenter() const
{
    return center_;
}


Triangle::Triangle(int xx, int yy):
    Shape(xx, yy)
{}

void Triangle::draw() const
{
    std::cout << "tria(" << center_.first << "," << center_.second << ") ";
}

Square::Square(int xx, int yy):
    Shape(xx, yy)
{}

void Square::draw() const
{
    std::cout << "squa(" << center_.first << "," << center_.second << ") ";
}

Circle::Circle(int xx, int yy):
    Shape(xx, yy)
{}

void Circle::draw() const
{
    std::cout << "circ(" << center_.first << "," << center_.second << ") ";
}

