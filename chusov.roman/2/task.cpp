#include "task.h"
#include <fstream>
#include <stdexcept>

#define SPACE_SYMBOL_SPACE ' '
#define SPACE_SYMBOL_NEW_LINE '\n'
#define SPACE_SYMBOL_TAB '\t'
#define IS_SPACE_SYMBOL(A) ((A == SPACE_SYMBOL_SPACE) || (A == SPACE_SYMBOL_NEW_LINE) || (A == SPACE_SYMBOL_TAB))

#define WORD_VAU "VAU!!!"
#define WORD_VAU_LEN 6
#define MAX_WORD_LEN 10
#define MAX_SIZE_OF_STRING 40

bool isPunctuationMark(const char & symbol);
void replaceByVAU(std::string::iterator & it, std::string &str);
void insertSpaceBetweenLetterAndPunctuationMark(std::string::iterator & it, std::string::iterator & itLast, std::string & str);
void deleteSpaceBetweenLetterAndPunctuationMark(std::string::iterator & it, std::string::iterator & itLast, std::string & str);

std::string & readText(const char * fileName, std::string & text)
{
    std::ifstream fin(fileName);

    if(fin.is_open())
    {
        std::string buffer;
        while(fin.good())
        {
            fin >> buffer;
            text += (buffer + SPACE_SYMBOL_SPACE);
            buffer.clear();
        }
    }

    return text;
}


std::string & formatText(std::string & text)
{
    std::string::iterator it = text.begin(), itLast = it;

    while(it != text.end())
    {
        if(it != itLast)
        {
            deleteSpaceBetweenLetterAndPunctuationMark(it, itLast, text);
            insertSpaceBetweenLetterAndPunctuationMark(it, itLast, text);
        }

        if(!IS_SPACE_SYMBOL(*it) && !isPunctuationMark(*it))
        {
            replaceByVAU(it, text);
        }

        itLast = it;
        ++it;
    }

    return text;
}

void convertText(std::string & text, VECTOR_OF_STRINGS & vect)
{
    std::string str;

    unsigned lettersInWordCounter = 0;
    std::string::iterator it = text.begin();

    while(it != text.end())
    {
        if(!IS_SPACE_SYMBOL(*it))
        {
            ++lettersInWordCounter;

            if(lettersInWordCounter > MAX_SIZE_OF_STRING)
            {
                throw std::length_error("letters in word > 40");
            }
        }
        else
        {
            if(str.size() >= MAX_SIZE_OF_STRING)
            {
                vect.push_back(str.substr(0, str.size() - lettersInWordCounter));
                it -= lettersInWordCounter;
                str.clear();
            }

            lettersInWordCounter = 0;
        }

        str.push_back(*it);
        ++it;
    }

    vect.push_back(str);
}

/**************/
void replaceByVAU(std::string::iterator & it, std::string & str)
{
    int lettersInWordCounter = 0;
    while(!IS_SPACE_SYMBOL(*it) && !isPunctuationMark(*it) && it != str.end())
    {
        ++lettersInWordCounter;
        ++it;
    }

    if(lettersInWordCounter >= MAX_WORD_LEN)
    {
        str.replace(it - lettersInWordCounter, it, WORD_VAU, WORD_VAU_LEN);
        it = it - WORD_VAU_LEN - 1;
    }
}

bool isPunctuationMark(const char & symbol)
{
    const std::string punctuationMarks(",.!?:;");

    return std::string::npos != punctuationMarks.find(symbol);
}

void deleteSpaceBetweenLetterAndPunctuationMark(std::string::iterator & it, std::string::iterator & itLast, std::string & str)
{
    if(IS_SPACE_SYMBOL(*itLast) && isPunctuationMark(*it))
    {
        str.erase(itLast);
        itLast = it - 1;
    }
}

void insertSpaceBetweenLetterAndPunctuationMark(std::string::iterator & it, std::string::iterator & itLast, std::string & str)
{
    if(isPunctuationMark(*itLast) && !isPunctuationMark(*it) && !IS_SPACE_SYMBOL(*it))
    {
        it = str.insert(it, SPACE_SYMBOL_SPACE);
        itLast = it - 1;
    }
}
