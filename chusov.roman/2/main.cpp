#include "task.h"
#include <iostream>

int main()
{  
    std::string text;
    VECTOR_OF_STRINGS vect;
    readText("Inputfile.txt", text);

    formatText(text);

    try
    {
        convertText(text, vect);
    }
    catch(std::exception & expt)
    {
        std::cerr << expt.what() << std::endl;
    }

    for(VECTOR_OF_STRINGS::iterator it = vect.begin(); it != vect.end(); ++it)
    {
        std::cout << *it << std::endl;
    }

    return 0;
}

