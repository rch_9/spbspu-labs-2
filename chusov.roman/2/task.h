#ifndef READFILE
#define READFILE

#include <string>
#include <vector>

std::string & readText(const char * fileName, std::string & text);

std::string & formatText(std::string & text);

typedef std::vector<std::string> VECTOR_OF_STRINGS;

void convertText(std::string & text, VECTOR_OF_STRINGS & vect);

#endif // READFILE

