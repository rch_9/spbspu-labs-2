TEMPLATE = app
#CONFIG += console c++03
QMAKE_CXXFLAGS += -std=c++03
#CONFIG += -Wall
#CONFIG += -Werror

CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    task.cpp

HEADERS += \
    task.h

DISTFILES += \
    ../build-2-Desktop_Qt_5_5_1_MinGW_32bit-Debug/Outputfile.txt \
    ../build-2-Desktop_Qt_5_5_1_GCC_64bit-Debug/Inputfile.txt

OTHER_FILES +=

